/**
 Copyright (c) 2016 ETH Zurich, 2013-2016 Oliver Hilsenbeck
  
 This file is part of the linear time Maximally Stable Extremal Regions (MSER)
 implementation (based on Nist�r and Stew�nius, 2008) by Oliver Hilsenbeck.
  
 This implementation is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Linear time maximally stable extremal regions detection
 *
 * Example usage:
 *
 *	// Assuming image is a pointer to an image with sizeY rows and sizeX columns
 *	mser::MSERDetector* d = new mser::MSERDetector(sizeX, sizeY);
 * 
 *	// Process image
 *	d->processImage(image, sizeX, sizeY, true);
 *
 *	// List seeds
 *	mser::seeds_t seeds;
 *	d->getMserSeeds(seeds);
 *	for(mser::seeds_t::const_iterator it = seeds.cbegin(); it != seeds.cend(); ++it) 
 *		cout << "MSER: x=" << *it / sizeX << ", y=" << *it % sizeX << endl;
 */

#ifndef mser_h__
#define mser_h__

// STL
#include <deque>
#include <utility>

// Internal
#include "mser_internal.h"


namespace mser {

	/**
	 * Forward declarations
	 */
	class MSERDetectorInternal;
	class MSERException;

	// Typedef for list of seeds
	typedef std::deque<unsigned int, AlignedAllocator<unsigned int> > seeds_t;

	// Typedef for list of pixels
	typedef std::deque<unsigned int, AlignedAllocator<unsigned int> > pixels_t; 

	/**
	 * MSER detector object
	 *
	 * Can be used to process an arbitrary number of pictures. The pictures do not need to
	 * have the same size. Parameters can be changed between runs. The MSERDetector instance can
	 * be created on the stack or on the heap, but it cannot be copied.
	 *
	 * Note on exceptions:
	 * Creating a MSERDetector object with operator new() can cause a std::bad_alloc exception, but in all other
	 * error cases and in all other functions only instances of MSERException are thrown. 
	 *
	 * Note on multi-threading:
	 * All functions are only thread safe if a different MSERDetector instance is created for each thread.
	 * You must never use one instance from more than one thread simultaneously, except for read-only operations.
	 * If mser::MSER_ALIGN_MEMORY is set to true, all used memory will be aligned according to the value
	 * of mser::MSER_CACHE_LINE_SIZE, which can help to prevent false sharing issues (currently only supported
	 * for MSVC compiler).
	 */
	class MSERDetector {
	public:

		/**
		 * Construct MSERDetectorAlign object. Throws MSERException if allocations fail.
		 * @param imageSizeX
		 */
		MSERDetector(unsigned int imageSizeX, unsigned int imageSizeY);

		/**
		 * Destructor
		 */
		~MSERDetector();

		/**
		 * Set parameters. Default values are set in constructor.
		 * @param delta the delta parameter, used for calculating the stability score (see below). Default: 5.
		 * @param minSize MSERs with more pixels will be discarded. Default: 3.
		 * @param maxSize MSERs with less pixels will be discarded. Default: 0.75 * imageSizeX * imageSizeY.
		 * @param maxVariation maximal stability score, calculated as: (|R[i+delta]|-|R[i]|) / |R[i]|. Default: 0.25.
		 */
		void setParameters(unsigned int delta, unsigned int minSize, unsigned int maxSize, float maxVariation);
		void setDelta(unsigned int delta);
		void setMaxSize(unsigned int maxSize);
		void setMinSize(unsigned int minSize);
		void setMaxVariation(float maxVariation);

		/**
		 * Get parameters (thread-safe)
		 */
		unsigned int getDelta() const;
		unsigned int getMaxSize() const;
		unsigned int getMinSize() const;
		float getMaxVariation() const;

		/**
		 * Process provided image
		 * @param imageData image data by row with one byte per pixel with graylevel values in [0, NUM_GRAY_LEVELS-1], where 0 stands for black.
		 * @param imageSizeX number of pixels in x-direction. Can vary between calls (causes reallocations if bigger).
		 * @param imageSizeY number of pixels in y-direction. Can vary between calls (causes reallocations if bigger).
		 * @param searchDarkOnBright if image should be scanned for dark on bright or bright on dark regions.
		 */
		void processImage(const unsigned char* imageData, unsigned int imageSizeX, unsigned int imageSizeY, bool searchDarkOnBright);

		///**
		// * Get number of found MSERs (thread-safe)
		// * @return number of MSERs that were found 
		// */
		//unsigned int getMsersCount() const;

		/**
		 * Get list of seeds for found MSERs as linear picture indexes (thread-safe)
		 * @param seeds reference to a std::deque object in which linear seed indexes of found MSERs will be saved
		 */
		void getMserSeeds(seeds_t& seeds) const;


		/**
		 * Get linear indexes of pixels belonging to region specified by linear seed index. Must not be called after processing another picture.
		 * Throws MSERException if parameters are invalid, e.g. if brightOnDark is set to true but was not set when calling processImage
		 * (NOT thread-safe).
		 * @param seed linear index of seed pixel
		 * @param pixels reference to a std::deque object in which linear pixel indexes of specified region will be saved
		 * @param darkOnBright if seed was obtained by dark on bright or bright on dark search
		 */
		void fillRegion(unsigned int seed, pixels_t& pixels, bool darkOnBright);

	private:
    
		// Internal detector instance
		MSERDetectorInternal *m_detector;

		// No copy
		MSERDetector(const MSERDetector&);
		MSERDetector& operator=(const MSERDetector&);
	};
}


#endif // mser_h__
