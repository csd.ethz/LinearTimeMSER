# Linear time MSER implementation #

Overview
--------
Linear time Maximally Stable Extremal Regions (MSER) C++ implementation with Matlab interface.

This is a C++ implementation of the linear time Maximally Stable Extremal Regions (MSER) algorithm described in Nist�r and Stew�nius (2008). See example.cpp for usage instructions.


References
----------
If you use this code in your research, please cite:

Hilsenbeck O, Schwarzfischer M, Loeffler D, Dimopoulos S, Hastreiter S, Marr C, Theis FJ and Schroeder T (2017) fastER: a user-friendly tool for ultrafast and robust cell segmentation in large-scale microscopy. Bioinformatics, in press.

Nist�r, D. and Stew�nius, H. (2008) Linear Time Maximally Stable Extremal Regions. In, Computer Vision - ECCV 2008. Springer Berlin Heidelberg, Berlin, Heidelberg, pp. 183-196.