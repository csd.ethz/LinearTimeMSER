/**
 Copyright (c) 2016 ETH Zurich, 2013-2016 Oliver Hilsenbeck
  
 This file is part of the linear time Maximally Stable Extremal Regions (MSER)
 implementation (based on Nist�r and Stew�nius, 2008) by Oliver Hilsenbeck.
  
 This implementation is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef mser_internal_h__
#define mser_internal_h__

// Project
#include "mser_defs.h"

// STL
#include <vector>
#include <deque>
#include <cassert>
#include <algorithm>
#include <cstddef>


namespace mser {

	/**
	 * Exception class
	 */
	class MSERException {
	public:

		/**
		 * Error types
		 */
		enum ErrorType {
			INVALID_PARAMETERS = 0,
			ALLOC_FAILED = 1,
			INTERNAL_ERROR = 2
		};

		/**
		 * Construct exception object, never throws.
		 * @param type error type, see error type constants.
		 */
		MSERException(ErrorType errorType) : m_errorType(errorType) {}

		/**
		 * Get error type, never throws.
		 * @return type of error, see error type constants.
		 */
		ErrorType getType() const {
			return m_errorType;
		}

	private:

		// Type of error
		const ErrorType m_errorType;
	};

	/**
	 * Base class for classes with aligned memory and MSERException in case of error
	 */
	class MSERBase {
	public:

		/**
		 * Allocation
		 * @param s number of bytes to allocate. Throws MSERException if allocation fails.
		 * @return valid pointer to allocated block.
		 */
		void* operator new(size_t s) 	{
			// Allocate possibly aligned memory
			void* ret = mser_malloc(s, mser::MSER_CACHE_LINE_SIZE);

			// Throw exception if allocation failed
			if(!ret)
				throw MSERException(MSERException::ALLOC_FAILED);

			// Allocation was successful
			return ret;
		}

		/**
		 * Deallocation
		 * @param pointer to memory block to be freed.
		 */
		void operator delete(void* p) 	{
			// Free possibly aligned memory
			mser_free(p);
		}

	};


	/**
	 * Allocator for STL containers with aligned memory. 
	 * Throws MSERException if allocation fails.
	 */
	template <typename T>
	class AlignedAllocator {
	public:

		// Typedefs
		typedef T value_type;
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;
		typedef T* pointer;
		typedef const T* const_pointer;
		typedef T& reference;
		typedef const T& const_reference;

		// Rebind
		template <typename U> 
		struct rebind { typedef AlignedAllocator<U> other; };

		// Get address
		pointer address(reference r) const { return &r; }
		const_pointer address(const_reference r) const { return &r; }

		// Construction, destruction
		AlignedAllocator() {}
		template <typename U> AlignedAllocator(const AlignedAllocator<U>& ) {}
		~AlignedAllocator() {}

		// Allocation
		pointer allocate(size_type n, typename std::allocator<void>::const_pointer = 0) {
			// Aligned allocation
			pointer ret = (pointer)mser_malloc(n * sizeof(T), MSER_CACHE_LINE_SIZE);
			if(!ret)
				throw MSERException(MSERException::ALLOC_FAILED);

			// Successful
			return ret;
		}

		void deallocate(pointer p, size_type n) {
			mser_free(p);
		}

		void construct(pointer p, const T& val) {
			new(p) T(val);
		}

		void destroy(pointer p) {
			p->~T();
		}

		// Max array size
		size_type max_size() const {
			size_type ret = ((size_type)-1) / sizeof(T);
			return (ret < 1 ? 1 : ret);
		}

		// Comparison
		bool operator==(AlignedAllocator const&) { return true; }
		bool operator!=(AlignedAllocator const& a) { return !operator==(a); }
	};


	/**
	 * MSERDetectorInternal
	 * Main class containing MSER implementation. All relevant memory blocks can be aligned according
	 * to the template parameter ALIGN.
	 */
	class MSERDetectorInternal : public MSERBase {
	public:

		/**
		 * Constructor
		 */
		MSERDetectorInternal(unsigned int imageSizeX, unsigned int imageSizeY);

		/**
		 * Destructor
		 */
		~MSERDetectorInternal();

		/**
		 * Process image
		 */
		void processImage(const unsigned char* imageData, unsigned int imageSizeX, unsigned int imageSizeY, bool darkOnBright);

		/**
		 * Get list of seeds for found MSERs
		 */
		void getMserSeeds(std::deque<unsigned int, AlignedAllocator<unsigned int> >& seeds) const;

		/**
		 * Fill region to get linear pixel indexes belonging to region
		 */
		void fillRegion(unsigned int seed, std::deque<unsigned int, AlignedAllocator<unsigned int> >& pixels, bool darkOnBright);

		// Parameters
		unsigned int m_delta;
		unsigned int m_minSize;
		unsigned int m_maxSize;
		float m_maxVariation;

	private:

		// Size of current image
		unsigned int m_imageSizeX;
		unsigned int m_imageSizeY;

		// Image data (only saved for fillRegion, only set if data was actually used in the last processImage call)
		const unsigned char* m_fillRegionsImageData;
		const unsigned char* m_fillRegionsImageDataInverted;

		// Image size used for memory allocations (can be greater than size of current image)
		unsigned int m_allocImageSizeX;
		unsigned int m_allocImageSizeY;

		// Visited pixels bitmask ()
		unsigned char* m_visitedPixels;

		// Found MSERs bitmask (with region seeds)
		unsigned char* m_MSERs;
		unsigned int m_numFoundMsers;

		// Buffer for inverted image data for bright on dark search
		unsigned char* m_invertedImageData;

		/**
		 * Process provided image
		 */
		void processImageInternal(const unsigned char* imageData, unsigned int imageSizeX, unsigned int imageSizeY);

		/**
		 * Update data structures according to m_imageSizeX and m_imageSizeY, throws MSERException in case of error
		 */
		void resetInternalStructures(bool brightOnDark);

		/**
		 * Prepare data structures for next image
		 */
		void prepareForNextImage(const unsigned char* imageData);

		/**
		 * Process stack
		 * @param newGrayLevel graylevel of next pixel
		 */
		void processStack(unsigned int newGrayLevel);

		/**
		 * Add MSER to list of detected MSERs
		 * @param seed the seed of the detected MSER
		 */
		void reportMser(unsigned int seed)
		{
			// Check if seed already saved
			if(testBit(m_MSERs, seed) == 0) {
				//std::cout << std::dec << "MSER: " << seed / m_imageSizeX << " - " << seed % m_imageSizeX << std::endl;

				setBit(m_MSERs, seed);
				++m_numFoundMsers;
			}
		}

		//////////////////////////////////////////////////////////////////////////
		// Heap data
		//////////////////////////////////////////////////////////////////////////

		// Stacks (one for each graylevel with size of pixels for that gray level),
		// store linear indexes of boundary pixels where darker pixels have higher priorities
		// along with one byte indicating the next edge to process (2 most significant bits).
		unsigned int* m_heapStacks;

		// Array of starting indexes of stacks for each graylevel in heapStacks
		int m_heapStacksIndexes[NUM_GRAY_LEVELS];

		// Array of number of elements currently in each stack
		int m_heapStacksLengths[NUM_GRAY_LEVELS];

		// Bitmask set to 1 for stacks with pixels
		mserUInt64 m_heapBitMask[NUM_GRAY_LEVELS / 64];

		// Number of bytes per element in the boundary pixels heap
		static const int BYTES_PER_HEAP_ELEMENT = 4;

		// Edge constants
		static const unsigned int EDGE_RIGHT = 0;
		static const unsigned int EDGE_DOWN = 1<<30;
		static const unsigned int EDGE_LEFT = 2<<30;
		static const unsigned int EDGE_UP = 3<<30;
		static const unsigned int EDGE_MASK = 3<<30;

		//////////////////////////////////////////////////////////////////////////
		// Heap functions
		//////////////////////////////////////////////////////////////////////////

		/**
		 * Get top element in heap and remove it from heap
		 * @param index receives linear index of top pixel in heap or -1 if heap is empty
		 * @param nextEdge next edge to process for pixel
		 * @return true if heap was not empty
		 */
		bool heapPopTopPixel(unsigned int& index, unsigned int& nextEdge) 	{
			// Get darkest pixel gray level
			int grayLevel = mser_bitScanReverse256(m_heapBitMask);
			if(grayLevel == -1)
				return false;

			// Get heap index
			int heapIndex = m_heapStacksIndexes[grayLevel] + m_heapStacksLengths[grayLevel] - 1;

			// Get pixel 
			index = m_heapStacks[heapIndex] & ~EDGE_MASK;
			nextEdge = m_heapStacks[heapIndex] & EDGE_MASK;

			// Decrease counter for gray level
			if(--m_heapStacksLengths[grayLevel] == 0) {
				// Unset bit
				m_heapBitMask[grayLevel / 64] &= ~((mserUInt64)1 << (grayLevel % 64));
			}

			return true;
		}

		/**
		 * Push pixel on heap
		 * @param index linear index of pixel in image
		 * @param grayLevel gray level of pixel
		 * @param nextEdge next edge to process
		 */
		void heapPushPixel(unsigned int index, unsigned int grayLevel, unsigned int nextEdge) {
			// Get heap index
			int heapIndex = m_heapStacksIndexes[grayLevel] + m_heapStacksLengths[grayLevel];

			// Store value
			m_heapStacks[heapIndex] = index | nextEdge;

			// Increase counter for pixels of gray level
			m_heapStacksLengths[grayLevel]++;

			// Ensure that corresponding bit is set to 1
			m_heapBitMask[grayLevel / 64] |= (mserUInt64)1 << (grayLevel % 64);
		}

		/**
		 * Get next edge. Order is always: RIGHT -> DOWN -> LEFT -> UP -> RIGHT -> ...
		 * @param edge the current edge constant
		 * @return next edge constant
		 */
		inline unsigned int getNextEdge(unsigned int edge)
		{
			unsigned int tmp = (edge >> 30) + 1;
			return tmp << 30;
		}

		/**
		 * Get pixel coordinate of specified neighbor
		 * @param pixel current pixel
		 * @param edge specifies which neighbor
		 * @return pixel coordinate or -1 if neighbor does not exist (for boundary pixels)
		 */
		inline unsigned int getNeighbor(unsigned int pixel, unsigned int edge)
		{
			switch(edge) {
			case EDGE_RIGHT:
				// Check if pixel is on right edge of image
				if(pixel % m_imageSizeX == m_imageSizeX - 1)
					return -1;
				return pixel + 1;
			case EDGE_DOWN:
				// Check if pixel is on bottom edge of image
				if(pixel / m_imageSizeX >= m_imageSizeY - 1)
					return -1;
				return pixel + m_imageSizeX;
			case EDGE_LEFT:
				// Check if pixel is on left edge of image
				if(pixel % m_imageSizeX == 0)
					return -1;
				return pixel - 1;
			case EDGE_UP:
				// Check if pixel is on top edge of image
				if(pixel < m_imageSizeX)
					return -1;
				return pixel - m_imageSizeX;
			default:
				throw MSERException(MSERException::INTERNAL_ERROR);
			}
		}

		//////////////////////////////////////////////////////////////////////////
		// Components
		//////////////////////////////////////////////////////////////////////////

		// Data structure describing one component
		struct Component {
			Component() : m_sizeHistory(mser::NUM_GRAY_LEVELS, 0), m_seedHistory(mser::NUM_GRAY_LEVELS, -1), m_grayLevel(0), m_minimumGrayLevel(0), m_numPixels(0) {}

			// Current graylevel of the component (is also the maximum graylevel for which this component may contain
			// pixels)
			int m_grayLevel;

			// Size history (number of pixels with gray level specified by index)
			std::vector<int,  AlignedAllocator<int> > m_sizeHistory;
			typedef std::vector<int,  AlignedAllocator<int> >::iterator size_iterator;

			// Seed history (for every graylevel for which this component has pixels, a seed pixel is saved)
			std::vector<int,  AlignedAllocator<int> > m_seedHistory;
			typedef std::vector<int,  AlignedAllocator<int> >::iterator seed_iterator;

			// Minimum gray level
			int m_minimumGrayLevel;

			// Total number of pixels (= sum of m_sizeHistory entries)
			int m_numPixels;
		};

		// Stack of components (+1 for dummy component)
		Component m_C[NUM_GRAY_LEVELS + 1];

		// Number of pixels currently in C
		int m_componentsCount;

		//////////////////////////////////////////////////////////////////////////
		// Component functions
		//////////////////////////////////////////////////////////////////////////

		/**
		 * Clear components stack
		 */
		void clearComponents() 	{
			// Clear seeds and size history of all components
			for(int i = 0; i < m_componentsCount; ++i) {
				for(int j = m_C[i].m_minimumGrayLevel; j <= std::min(m_C[i].m_grayLevel, static_cast<int>(m_C[i].m_seedHistory.size()-1)); ++j) {
					m_C[i].m_seedHistory[j] = -1;
					m_C[i].m_sizeHistory[j] = 0;
				}
				assert(checkArrayValues(m_C[i].m_sizeHistory.begin(), m_C[i].m_sizeHistory.end(), 0));
				assert(checkArrayValues(m_C[i].m_seedHistory.begin(), m_C[i].m_seedHistory.end(), -1));
			}

			// Set count to 0
			m_componentsCount = 0;
		}

		/**
		 * Debug helper: check if all values in [b, e[ have the same value val.                                                                  
		 */
		template <class It, class T>
		static bool checkArrayValues(It b, It e, T val) {
			while(b != e) {
				if(*b != val)
					return false;
				++b;
			}
			return true;
		}

		/**
		 * Add a component to the components stack
		 * @param grayLevel graylevel of component
		 */
		void addComponent(unsigned int grayLevel) 	{
			// Set data for top component
			m_C[m_componentsCount].m_grayLevel = grayLevel;
			m_C[m_componentsCount].m_minimumGrayLevel = grayLevel;
			m_C[m_componentsCount].m_numPixels = 0;

			//// Set size history to 0 for every gray level
			//for(std::vector<int, AlignedAllocator<int> >::iterator it = m_C[m_componentsCount].m_sizeHistory.begin(); it != m_C[m_componentsCount].m_sizeHistory.end(); ++it)
			//	*it = 0;
			assert(checkArrayValues(m_C[m_componentsCount].m_sizeHistory.begin(), m_C[m_componentsCount].m_sizeHistory.end(), 0));

			//// Set seed history to -1 for every gray level
			//for(std::vector<int, AlignedAllocator<int> >::iterator it = m_C[m_componentsCount].m_seedHistory.begin(); it != m_C[m_componentsCount].m_seedHistory.end(); ++it)
			//	*it = -1;
			assert(checkArrayValues(m_C[m_componentsCount].m_seedHistory.begin(), m_C[m_componentsCount].m_seedHistory.end(), -1));

			// Increase counter
			++m_componentsCount;
		}

		/**
		 * Add pixel to component on top of stack
		 * @param grayLevel graylevel of pixel to add
		 * @param pixel coordinate of pixel to add
		 */
		void addPixelToTopComponent(unsigned int grayLevel, unsigned int pixel) {
			// Get top component
			Component* topComponent = &m_C[m_componentsCount - 1];
			assert(topComponent->m_grayLevel == grayLevel);

			// Set seed
			topComponent->m_seedHistory[grayLevel] = pixel;

			// Increase pixel count
			++topComponent->m_sizeHistory[grayLevel];
			++topComponent->m_numPixels;
		}

		/**
		 * Merge the top component on the stack into the second one
		 */
		void mergeTopComponents();

	};

}
#endif // mser_internal_h__