/**
 Copyright (c) 2016 ETH Zurich, 2013-2016 Oliver Hilsenbeck
  
 This file is part of the linear time Maximally Stable Extremal Regions (MSER)
 implementation (based on Nist�r and Stew�nius, 2008) by Oliver Hilsenbeck.
  
 This implementation is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "mser_internal.h"

// C Library
#include <memory.h>

namespace mser {

	MSERDetectorInternal::MSERDetectorInternal(unsigned int imageSizeX, unsigned int imageSizeY)
	try {
		// Default parameters
		m_delta = 5;
		m_minSize = 3;
		m_maxSize = static_cast<unsigned int>(0.75f * imageSizeX * imageSizeY);
		m_maxVariation = 0.25f;

		// Init variables
		m_heapStacks = 0;
		m_visitedPixels = 0;
		m_MSERs = 0;
		m_allocImageSizeX = 0;
		m_allocImageSizeY = 0;
		m_invertedImageData = 0;
		m_imageSizeX = imageSizeX;
		m_imageSizeY = imageSizeY;
		m_componentsCount = 0;

		// Initial allocations
		resetInternalStructures(false);
	}
	// Handler to avoid memory leak in case of error, automatically re-throws to caller
	catch(const MSERException&) {
		// Cleanup
		mser_free(m_heapStacks);
		mser_free(m_visitedPixels);
		mser_free(m_MSERs);
	}

	
	mser::MSERDetectorInternal::~MSERDetectorInternal()
	{
		// Cleanup
		mser_free(m_heapStacks);
		mser_free(m_visitedPixels);
		mser_free(m_MSERs);
		mser_free(m_invertedImageData);
	}

	
	void mser::MSERDetectorInternal::resetInternalStructures(bool brightOnDark)
	{
		// Check if we have to reallocate m_invertedImageData
		if(brightOnDark) {
			if(!m_invertedImageData || m_imageSizeX * m_imageSizeY > m_allocImageSizeX * m_allocImageSizeY) {
				// Realloc
				mser_free(m_invertedImageData);
				m_invertedImageData = (unsigned char*)mser_malloc(m_imageSizeX * m_imageSizeY, mser::MSER_CACHE_LINE_SIZE);

				// Check for error
				if(!m_invertedImageData)
					throw MSERException(MSERException::ALLOC_FAILED);
			}
		}

		// Check if we have to reallocate anything else
		if(m_imageSizeX * m_imageSizeY <= m_allocImageSizeX * m_allocImageSizeY)
			return;

		// Free old data
		mser_free(m_heapStacks);
		mser_free(m_visitedPixels);
		mser_free(m_MSERs);

		// Allocations
		m_heapStacks = (unsigned int*)mser_malloc(m_imageSizeX * m_imageSizeY * BYTES_PER_HEAP_ELEMENT, mser::MSER_CACHE_LINE_SIZE);
		m_visitedPixels = (unsigned char*)mser_malloc((m_imageSizeX * m_imageSizeY + 7) / 8, mser::MSER_CACHE_LINE_SIZE);
		m_MSERs = (unsigned char*)mser_malloc((m_imageSizeX * m_imageSizeY + 7) / 8, mser::MSER_CACHE_LINE_SIZE);

		// Check for failed allocations
		if(!m_heapStacks || !m_visitedPixels || !m_MSERs)
			throw MSERException(MSERException::ALLOC_FAILED);

		// Update alloc image sizes
		m_allocImageSizeX = m_imageSizeX;
		m_allocImageSizeY = m_imageSizeY;
	}

	
	void mser::MSERDetectorInternal::processImage( const unsigned char* imageData, unsigned int imageSizeX, unsigned int imageSizeY, bool darkOnBright)
	{
		// Check parameters
		if(!imageData || !imageSizeX || !imageSizeY)
			throw MSERException(MSERException::INVALID_PARAMETERS);

		// Reset pointers to image data
		m_fillRegionsImageData = 0;
		m_fillRegionsImageDataInverted = 0;

		// Store data
		m_imageSizeX = imageSizeX;
		m_imageSizeY = imageSizeY;

		// Reallocate if necessary
		resetInternalStructures(!darkOnBright);

		// Reset MSERs 
		memset(m_MSERs, 0, (m_allocImageSizeX * m_allocImageSizeY+7)/8);
		m_numFoundMsers = 0;

		// Dark on bright
		if(darkOnBright) {
			// Prepare for processing
			prepareForNextImage(imageData);

			// Process image
			processImageInternal(imageData, imageSizeX, imageSizeY);

			// Remember for fillRegions
			m_fillRegionsImageData = imageData;
		}
		else {
			// Invert image
			for(unsigned int i = 0; i < imageSizeX * imageSizeY; ++i) 
				m_invertedImageData[i] = ~imageData[i];		// 255 - imageData[i]

			// Prepare for processing
			prepareForNextImage(m_invertedImageData);

			// Process image
			processImageInternal(m_invertedImageData, imageSizeX, imageSizeY);

			// Remember for fillRegions
			m_fillRegionsImageDataInverted = m_invertedImageData;
		}
	}


	void MSERDetectorInternal::processImageInternal( const unsigned char* imageData, unsigned int imageSizeX, unsigned int imageSizeY )
	{
		// Dummy component
		addComponent(mser::NUM_GRAY_LEVELS);

		// Process pixels starting with first
		unsigned int currentPixel = 0;
		unsigned int currentGrayLevel = imageData[currentPixel];
		unsigned int currentEdge = EDGE_UP;
		setBit(m_visitedPixels, currentPixel);

MainLoopStart:

		// Push empty component on stack with currentGrayLevel
		addComponent(currentGrayLevel);

		while(1) {
			// Evaluate neighbors (clockwise, EDGE_UP is always last)
			do {
				currentEdge = getNextEdge(currentEdge);

				// Get neighbor pixel index
				unsigned int neighbor = getNeighbor(currentPixel, currentEdge);
				if(neighbor != -1) {
					// Check if neighbor already accessible
					if(!testBit(m_visitedPixels, neighbor)) {
						// Mark as accessible
						setBit(m_visitedPixels, neighbor);

						// Check neighbor gray level
						unsigned int neighborGrayLevel = imageData[neighbor];
						if(neighborGrayLevel >= currentGrayLevel) {
							heapPushPixel(neighbor, neighborGrayLevel, EDGE_UP);
						}
						else {
							// Put current pixel back on stack (with current edge, after pop, next edge will be selected)
							heapPushPixel(currentPixel, currentGrayLevel, currentEdge);

							// Consider neighbor
							currentPixel = neighbor;
							currentGrayLevel = neighborGrayLevel;
							currentEdge = EDGE_UP;

							// Go to outer loop
							goto MainLoopStart;
						}
					}
				}
			} while(currentEdge != EDGE_UP);

			// Add current pixel to current component
			addPixelToTopComponent(currentGrayLevel, currentPixel);
			//m_C[m_componentsCount - 1].pixels.push_back(currentPixel);

			// Get next pixel and edge from heap
			if(!heapPopTopPixel(currentPixel, currentEdge))
				return;

			// Get next gray level
			unsigned int newGrayLevel = imageData[currentPixel];
			if(newGrayLevel != currentGrayLevel) {
				// newGrayLevel must be greater than currentGrayLevel -> process stack
				processStack(newGrayLevel);

				// Process next pixel
				currentGrayLevel = newGrayLevel;
			}
		}
	}
	

	void mser::MSERDetectorInternal::prepareForNextImage(const unsigned char* imageData)
	{
		/**
		 * Start by preparing the heap
		 */

		// Set heap stack lengths and bitmasks to 0
		memset(m_heapStacksLengths, 0, sizeof(m_heapStacksLengths));
		memset(m_heapBitMask, 0, sizeof(m_heapBitMask));
		memset(m_visitedPixels, 0, (m_allocImageSizeX * m_allocImageSizeY+7)/8);

		// Create histogram of pixel intensities and store it in m_heapStacksIndexes
		memset(m_heapStacksIndexes, 0, sizeof(m_heapStacksIndexes));
		for(unsigned int i = 0; i < m_imageSizeX * m_imageSizeY; ++i)
			++m_heapStacksIndexes[imageData[i]];

		// Calculate m_heapStacksIndexes values, starting with gray level 0
		unsigned int startIndex = 0;
		for(unsigned int i = 0; i < NUM_GRAY_LEVELS; ++i) {
			// Remember count of gray level i
			unsigned int tmp = m_heapStacksIndexes[i];

			// Set start index for gray level i
			m_heapStacksIndexes[i] = startIndex;

			// Calc start index for gray level i + 1
			startIndex += tmp;
		}

		/**
		 * Clear components stack
		 */
		clearComponents();
	}

	void MSERDetectorInternal::processStack( unsigned int newGrayLevel )
	{
		do {
			/**
			 * Check if top component is MSER
			 */
			Component* topComp = &m_C[m_componentsCount - 1]; 

			// Size of R(i-delta) and R(i)
			unsigned int sizeAccumulated = 0, sizePlusDeltaAccumulated = 0;

			// Iterate over graylevels range from min gray level of top component to next graylevel
			unsigned int grayLimit = std::min(newGrayLevel, static_cast<unsigned int>(m_C[m_componentsCount - 2].m_grayLevel));
			unsigned int counter = 0;

			// Remember the growth rate of the last iteration and if that one was not bigger than the one before that.
			// If so, it is maximally stable if it also not bigger than the current growth rate
			float prevGrowthRate = -1.0f;
			bool prevCouldBeMser = true;
			int prevSeed = -1;
			unsigned int prevSize = 0;

			for(unsigned int i = topComp->m_minimumGrayLevel; i <= grayLimit; ++i) {
				// Size for +delta region
				sizePlusDeltaAccumulated += topComp->m_sizeHistory[i];

				// If we reached delta iterations, we can start looking for MSERs
				if(counter >= m_delta) {
					// Size for +0 region
					sizeAccumulated += topComp->m_sizeHistory[i - m_delta];

					// Calc current growth rate
					float growthRate = (sizePlusDeltaAccumulated - sizeAccumulated) / (float)sizeAccumulated;

					// If we have a previous growth rate, check if previous region is an MSER
					if(prevGrowthRate >= 0) {
						// Check if previous had no bigger growth rate than that before
						// and if previous growth rate is equal or smaller than current -> MSER detected
						if(prevCouldBeMser && prevGrowthRate <= growthRate && prevGrowthRate <= m_maxVariation && prevSize >= m_minSize && prevSize <= m_maxSize) {

							//// Debug
							//if(!testBit(m_MSERs, prevSeed + 1)) {
							//	std::cout << std::dec << "MSER: " << prevSeed / m_imageSizeX << " - " << prevSeed % m_imageSizeX << " - size: " << prevSize << std::endl;
							//}

							// MSER meeting all specified requirements found
							reportMser(prevSeed);
						}

						// Check if this growth rate is smaller than previous
						if(prevGrowthRate < growthRate) 
							prevCouldBeMser = false;
						else
							prevCouldBeMser = true;
					}

					// Remember current growth rate and size
					prevGrowthRate = growthRate;
					prevSize = sizeAccumulated;

					// Remember seed for current growth rate (if seed is -1, the component contains no pixels at
					// i - m_delta and therefore the old seed remains valid)
					if(topComp->m_seedHistory[i - m_delta] != -1) {
						prevSeed = topComp->m_seedHistory[i - m_delta];

						assert(topComp->m_sizeHistory[i - m_delta] != 0);
					}
					else {
						assert(topComp->m_sizeHistory[i - m_delta] == 0);
					}
				}

				counter++;
			}

			/**
			 * Rest of processStack function
			 */

			// Compare newGrayLevel with graylevel of second component on stack
			if(newGrayLevel < static_cast<unsigned int>(m_C[m_componentsCount - 2].m_grayLevel)) {
				// No component exists for newGrayLevel, so increase top component's graylevel to newGrayLevel,
				// pixel with newGrayLevel will be added to existing top component then
				m_C[m_componentsCount - 1].m_grayLevel = newGrayLevel;
				return;
			}

			// Merge the two topmost components
			mergeTopComponents();
		} while(newGrayLevel > static_cast<unsigned int>(m_C[m_componentsCount - 1].m_grayLevel));
	}


	void MSERDetectorInternal::mergeTopComponents()
	{
		// Get components to merge
		Component* topC = &m_C[m_componentsCount - 1];
		Component* secC = &m_C[m_componentsCount - 2];

		assert(topC->m_grayLevel < secC->m_grayLevel);

		//// Check if topC and secC do not overlap
		//if(topC->m_grayLevel < secC->m_minimumGrayLevel) {
		//	// topC and secC contain no pixels at the same gray level (they don't overlap) so
		//	// we have to merge topC into secC

		//	// Take seeds from topC
		//	for(int i = topC->m_minimumGrayLevel; i <= topC->m_grayLevel; ++i) {
		//		assert(secC->m_seedHistory[i] == -1);
		//		secC->m_seedHistory[i] = topC->m_seedHistory[i];
		//	}

		//	// Take size history from topC
		//	for(int i = topC->m_minimumGrayLevel; i <= topC->m_grayLevel; ++i) {
		//		assert(secC->m_sizeHistory[i] == 0);
		//		secC->m_sizeHistory[i] = topC->m_sizeHistory[i];
		//	}
		//	secC->m_numPixels += topC->m_numPixels;

		//	// Take over minimum gray level from topC
		//	secC->m_minimumGrayLevel = topC->m_minimumGrayLevel;
		//}
		//else {
		// Determine 'winner' by size and takeover its seed history
		int sizeTop = topC->m_numPixels;
		int sizeSec = secC->m_numPixels - secC->m_sizeHistory[secC->m_grayLevel];
		if(sizeTop >= sizeSec) {
			// Top component topC is winner -> overwrite seed history, size history and minimum gray level of secC
			// with the ones of topC up to (but without) secC->m_grayLevel

			// Lowest gray level of both components (ToDo: it would be sufficient to start from topC->m_minimumGrayLevel though?)
			//int startGrayLevel = std::min(topC->m_minimumGrayLevel, secC->m_minimumGrayLevel);
			//unsigned int startGrayLevel = topC->m_minimumGrayLevel;

			// Take over seeds history
			for(int i = topC->m_minimumGrayLevel; i < secC->m_grayLevel; ++i) {
				secC->m_seedHistory[i] = topC->m_seedHistory[i];
				topC->m_seedHistory[i] = -1;
			}
			assert(checkArrayValues(topC->m_seedHistory.begin(), topC->m_seedHistory.end(), -1));

			// Take over size history
			for(int i = topC->m_minimumGrayLevel; i < secC->m_grayLevel; ++i) {
				secC->m_sizeHistory[i] = topC->m_sizeHistory[i];
				topC->m_sizeHistory[i] = 0;
			}
			assert(checkArrayValues(topC->m_sizeHistory.begin(), topC->m_sizeHistory.end(), 0));

			// Reset size and seed history of secC up to new graylevel
			for(int i = secC->m_minimumGrayLevel; i < topC->m_minimumGrayLevel; ++i) {
				secC->m_seedHistory[i] = -1;
				secC->m_sizeHistory[i] = 0;
			}

			// Takeover minimum gray level
			secC->m_minimumGrayLevel = topC->m_minimumGrayLevel;

			// Size of secC jumps at secC->m_grayLevel
			secC->m_sizeHistory[secC->m_grayLevel] += sizeSec;

			// Update size
			secC->m_numPixels += topC->m_numPixels;
		}
		else {
			// Second component secC is winner -> seed history, size history and minimum gray level of secC
			// remain unchanged, but size of secC jumps at secC->m_grayLevel by the number of pixels in topC
			secC->m_sizeHistory[secC->m_grayLevel] += topC->m_numPixels;
			secC->m_numPixels += topC->m_numPixels;

			// Clear seed and size histories of top component
			// Take over seeds history
			for(int i = topC->m_minimumGrayLevel; i <= topC->m_grayLevel; ++i) {
				topC->m_seedHistory[i] = -1;
			}
			assert(checkArrayValues(topC->m_seedHistory.begin(), topC->m_seedHistory.end(), -1));

			// Take over size history
			for(int i = topC->m_minimumGrayLevel; i <= topC->m_grayLevel; ++i) {
				topC->m_sizeHistory[i] = 0;
			}
			assert(checkArrayValues(topC->m_sizeHistory.begin(), topC->m_sizeHistory.end(), 0));
		}
		//}

		

		// Top component removed
		--m_componentsCount;
	}

	void MSERDetectorInternal::getMserSeeds(std::deque<unsigned int, AlignedAllocator<unsigned int> >& seeds) const
	{
		// Number of found seeds
		unsigned int found = 0;
		for(unsigned int i = 0; i < m_imageSizeX * m_imageSizeY; ++i) {
			if(testBit(m_MSERs, i)) {
				// Add to returned list
				seeds.push_back(i);

				// Check if we found all
				if(++found >= m_numFoundMsers)
					break;
			}
		}
	}


	void MSERDetectorInternal::fillRegion( unsigned int seed, std::deque<unsigned int, AlignedAllocator<unsigned int> >& pixels, bool darkOnBright )
	{
		// Check parameters
		if((darkOnBright && !m_fillRegionsImageData) 
			|| (!darkOnBright && !m_fillRegionsImageDataInverted)
			|| seed >= m_imageSizeX * m_imageSizeY)
			throw MSERException(MSERException::INVALID_PARAMETERS);

		// Data we will use
		const unsigned char* data;
		if(darkOnBright)
			data = m_fillRegionsImageData;
		else
			data = m_fillRegionsImageDataInverted;

		// Reset bitmask of visited pixels
		memset(m_visitedPixels, 0, (m_allocImageSizeX * m_allocImageSizeY+7)/8);

		// Current number of elements in heap (which we will abuse for this)
		unsigned int heapCount = 0;

		// Fill region by using heap similar to processImage()
		unsigned int currentPixel = seed,
			threshold = data[currentPixel],
			currentEdge = EDGE_UP;
		setBit(m_visitedPixels, currentPixel);
		while(1) {
			// Evaluate neighbors (clockwise, EDGE_UP is always last)
			do {
				currentEdge = getNextEdge(currentEdge);

				// Get neighbor pixel index
				unsigned int neighbor = getNeighbor(currentPixel, currentEdge);
				if(neighbor != -1) {
					// Check if neighbor already accessible
					if(!testBit(m_visitedPixels, neighbor)) {
						// Mark as accessible
						setBit(m_visitedPixels, neighbor);

						// Check neighbor gray level and ignore pixels with higher gray levels
						unsigned int neighborGrayLevel = data[neighbor];
						if(neighborGrayLevel <= threshold) {
							m_heapStacks[heapCount++] = neighbor;
							//heapPushPixel(neighbor, neighborGrayLevel, EDGE_UP);
						}
					}
				}
			} while(currentEdge != EDGE_UP);

			// Debug check
			assert(currentPixel / m_imageSizeX < m_imageSizeY);

			// Add current pixel to pixels list
			pixels.push_back(currentPixel);

			// Get next pixel from heap
			if(!heapCount) {
				assert(pixels.size() <= m_maxSize && pixels.size() >= m_minSize);
				return;
			}

			currentPixel = m_heapStacks[--heapCount];
		}
	}

}