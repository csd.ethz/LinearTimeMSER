/**
 Copyright (c) 2016 ETH Zurich, 2013-2016 Oliver Hilsenbeck
  
 This file is part of the linear time Maximally Stable Extremal Regions (MSER)
 implementation (based on Nist�r and Stew�nius, 2008) by Oliver Hilsenbeck.
  
 This implementation is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
  
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "mser.h"
#include "mser_internal.h"


namespace mser {

	MSERDetector::MSERDetector( unsigned int imageSizeX, unsigned int imageSizeY )
	{
		//// Init variables
		//fillRegionsVisited = 0;
		//fillRegionsVisitedSize = 0;

		// Create detector
		m_detector = new MSERDetectorInternal(imageSizeX, imageSizeY);
		if(!m_detector)
			throw MSERException(MSERException::ALLOC_FAILED);
	}

	MSERDetector::~MSERDetector()
	{
		delete m_detector;
		//mser_free(fillRegionsVisited);
	}

	void MSERDetector::setParameters(unsigned int delta, unsigned int minSize, unsigned int maxSize, float maxVariation)
	{
		// Redirect to setters
		setDelta(delta);
		setMaxSize(maxSize);
		setMinSize(minSize);
		setMaxVariation(maxVariation);
	}

	void MSERDetector::setDelta( unsigned int delta )
	{
		// Basic parameter checks
		if(delta < 1)
			throw MSERException(MSERException::INVALID_PARAMETERS);

		m_detector->m_delta = delta;
	}

	void MSERDetector::setMaxSize( unsigned int maxSize )
	{
		m_detector->m_maxSize = maxSize;
	}

	void MSERDetector::setMinSize( unsigned int minSize )
	{
		m_detector->m_minSize = minSize;
	}

	void MSERDetector::setMaxVariation( float maxVariation )
	{
		// Basic parameter checks
		if(maxVariation < 0)
			throw MSERException(MSERException::INVALID_PARAMETERS);

		m_detector->m_maxVariation = maxVariation;
	}

	unsigned int MSERDetector::getDelta() const
	{
		return m_detector->m_delta;
	}

	unsigned int MSERDetector::getMaxSize() const
	{
		return m_detector->m_maxSize;
	}

	unsigned int MSERDetector::getMinSize() const
	{
		return m_detector->m_minSize;
	}

	float MSERDetector::getMaxVariation() const
	{
		return m_detector->m_maxVariation;
	}

	void MSERDetector::processImage(const unsigned char* imageData, unsigned int imageSizeX, unsigned int imageSizeY, bool searchDarkOnBright )
	{
		m_detector->processImage(imageData, imageSizeX, imageSizeY, searchDarkOnBright);
	}

	void MSERDetector::getMserSeeds(seeds_t& seeds) const
	{
		m_detector->getMserSeeds(seeds);
	}

	void MSERDetector::fillRegion( unsigned int seed, std::deque<unsigned int, AlignedAllocator<unsigned int> >& pixels, bool darkOnBright )
	{
		m_detector->fillRegion(seed, pixels, darkOnBright);
	}

}