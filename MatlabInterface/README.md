Compile:
--------
Copy all code files into one directory and enter the following command in Matlab:

mex linearMser.cpp mser.cpp mser_internal.cpp


Test:
-----

I = imread('image.png');
msers = linearMser(I, 5, 10, 10000, 0.5, true);
s = size(msers);
I2 = I;
for i=1:s(1)
 I2(msers{i}) = 255;
end
imshow(I2);